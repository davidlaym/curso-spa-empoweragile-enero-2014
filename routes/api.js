var dataStore = require('../api/jsonReader.js');
var __routeBase = '/api/people';
var  __returnJSON = function(object,res) {
    var json = JSON.stringify(object);
    res.statusCode = 200;
    res.writeHead(200, {"Content-Type": "application/json"});
    res.write(json);
    res.end();
}

exports.setup = function (app) {
    app.get(__routeBase, function (req, res) {
        var object = dataStore.getPeople();
        __returnJSON(object,res);
    });
    app.get(__routeBase + '/:id', function (req, res) {
        var object = dataStore.getPerson(req.params.id);
        __returnJSON(object,res);
    });
    app.post(__routeBase, function (req, res) {

        var id= dataStore.createPerson(req.body);
        res.statusCode=201;

        // esto no es necesario, pero es lo que el protocolo
        // http dicta que se debe hacer
        res.writeHead({"Location": "#/details/"+id});

        res.end()

    });
    app.put(__routeBase+ '/:id',function(req,res){
        dataStore.savePerson(req.body);
        res.statusCode= 200;
        res.end();
    });
}