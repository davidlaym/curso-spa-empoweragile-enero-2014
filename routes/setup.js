var public_redirect = require('./public_redirect.js');
var api = require('./api.js');

exports.setup=function(app ) {
    public_redirect.setup(app);

    api.setup(app);
}