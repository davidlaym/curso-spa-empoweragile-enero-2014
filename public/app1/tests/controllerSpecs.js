describe('Binding manual', function() {

  var $scope = null;
  var ctrl = null;

  beforeEach(module('app'));

  beforeEach(inject(function($rootScope, $controller) {
    $scope = $rootScope.$new();

    ctrl = $controller('AppController', {
      $scope: $scope
    });
  }));

  it('Cuando el boton es presionado, deberia asignar el valor' 
      + 'de la variable text a la variable result', function() {
    $scope.text = "valor predeterminado";
    $scope.changeValue();
    expect($scope.result).toEqual($scope.text);
    
  });
});