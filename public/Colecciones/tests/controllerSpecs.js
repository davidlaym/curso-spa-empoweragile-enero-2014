
describe('App Coleciones2, prueba nok', function() {
    var $scope = null;
  var ctrl = null;
  var createController = null;
   var $httpBackend = null;

  beforeEach(module('app'));

  beforeEach(inject(function($rootScope, $controller, $injector) {
    $scope = $rootScope.$new();
    $httpBackend = $injector.get('$httpBackend');
    $httpBackend.when('GET', '../data/people.json').respond(404, 'not found ');
    
    createController = function(){ 
      ctrl = $controller('Colecciones2Controller', {
        $scope: $scope
      });
    };
  }));

   it('Validar error 404', function() {
    //Valida
    createController();
    $httpBackend.flush();
    expect($scope.arrPersonas).toEqual([]);  
  });

});


describe('App Coleciones2', function() {
  
  var $scope = null;
  var ctrl = null;
  var createController = null;
   var $httpBackend = null;

  beforeEach(module('app'));

  beforeEach(inject(function($rootScope, $controller, $injector) {
    $scope = $rootScope.$new();
    $httpBackend = $injector.get('$httpBackend');
     $httpBackend.when('GET', '../data/people.json').respond(
        [
            {
                "id": 0,
                "isActive": true,
                "image": "http://placekitten.com/32/32",
                "age": 30,
                "name": "Maura Carpenter",
                "gender": "female",
                "company": "Flum",
                "email": "mauracarpenter@flum.com",
                "phone": "+1 (805) 537-2583",
                "address": "452 Clermont Avenue, Dotsero, New York, 7718",
                "about": "Fugiat laborum tempor dolor mollit. In non dolor fugiat est. Aliqua commodo dolor nostrud ea incididunt ut aute anim elit consectetur et magna.\r\n",
                "registered": "1994-09-27T18:43:51 +03:00",
                "latitude": 46.760498,
                "longitude": 177.032466
            },
            {
                "id": 1,
                "isActive": false,
                "image": "http://placekitten.com/32/32",
                "age": 21,
                "name": "Hickman Emerson",
                "gender": "male",
                "company": "Quadeebo",
                "email": "hickmanemerson@quadeebo.com",
                "phone": "+1 (834) 496-3937",
                "address": "639 Decatur Street, Edgewater, Nebraska, 3572",
                "about": "Nulla do tempor do dolor ex amet do esse duis ad tempor dolore. Deserunt incididunt tempor cupidatat cillum reprehenderit duis proident pariatur ullamco quis labore Lorem qui. Id sit ea ipsum dolor enim ipsum elit nostrud ea elit irure in. Proident ad non aliquip velit elit exercitation sint reprehenderit consectetur. Sint irure in nulla anim reprehenderit laboris occaecat anim cillum veniam incididunt eiusmod quis id.\r\n",
                "registered": "1991-01-23T21:36:04 +03:00",
                "latitude": -23.766016,
                "longitude": 166.051566
            },
            {
                "id": 2,
                "isActive": false,
                "image": "http://placekitten.com/32/32",
                "age": 36,
                "name": "Henson Mcneil",
                "gender": "male",
                "company": "Insurity",
                "email": "hensonmcneil@insurity.com",
                "phone": "+1 (910) 410-2287",
                "address": "910 Poly Place, Shindler, Missouri, 4260",
                "about": "Lorem cupidatat duis duis deserunt incididunt duis pariatur qui officia ullamco consequat sint veniam. Occaecat sit non aute laborum eiusmod excepteur culpa excepteur pariatur sint ad cillum. Et quis nisi reprehenderit dolore. Anim proident non amet consectetur commodo.\r\n",
                "registered": "2011-02-06T14:55:05 +03:00",
                "latitude": 68.712413,
                "longitude": -14.692969
            },
            {
                "id": 3,
                "isActive": true,
                "image": "http://placekitten.com/32/32",
                "age": 39,
                "name": "Mattie Knowles",
                "gender": "female",
                "company": "Digial",
                "email": "mattieknowles@digial.com",
                "phone": "+1 (842) 524-2934",
                "address": "745 Schenectady Avenue, Denio, Oklahoma, 4389",
                "about": "Qui id reprehenderit enim fugiat. Sint et nostrud ad reprehenderit laborum non sint fugiat labore pariatur ipsum. Tempor ut ad amet sit ut sint adipisicing consectetur aliquip consequat.\r\n",
                "registered": "1993-04-25T08:07:26 +04:00",
                "latitude": -42.309732,
                "longitude": -149.057205
            },
            {
                "id": 4,
                "isActive": true,
                "image": "http://placekitten.com/32/32",
                "age": 22,
                "name": "Vaughn Suarez",
                "gender": "male",
                "company": "Unia",
                "email": "vaughnsuarez@unia.com",
                "phone": "+1 (900) 410-3566",
                "address": "543 Imlay Street, Shrewsbury, Washington, 1974",
                "about": "Velit deserunt laboris ut occaecat deserunt aliquip velit aliquip. Anim excepteur velit pariatur ipsum consequat laborum culpa adipisicing pariatur reprehenderit esse. Ut eiusmod et culpa commodo velit qui amet nisi. Quis aliquip occaecat sit cillum non sunt eu laborum enim ea. Amet ipsum Lorem reprehenderit sit ea mollit est sunt velit in.\r\n",
                "registered": "2009-01-07T02:36:50 +03:00",
                "latitude": 31.553048,
                "longitude": -130.138809
            },
            {
                "id": 5,
                "isActive": true,
                "image": "http://placekitten.com/32/32",
                "age": 26,
                "name": "Amy Rhodes",
                "gender": "female",
                "company": "Quotezart",
                "email": "amyrhodes@quotezart.com",
                "phone": "+1 (857) 599-3578",
                "address": "972 Cypress Avenue, Chase, Wisconsin, 5721",
                "about": "Laboris amet deserunt fugiat eiusmod in sint excepteur officia dolore mollit. Ex ea sit magna cupidatat non elit occaecat enim aliqua adipisicing magna nostrud excepteur. Labore deserunt sunt commodo veniam irure ea veniam. Sint et do aliqua nisi. Mollit do in duis eu veniam tempor excepteur exercitation. Et cupidatat aute sit ipsum.\r\n",
                "registered": "2002-08-21T07:29:45 +04:00",
                "latitude": 74.540662,
                "longitude": 114.929006
            }
        ]

      );
    
    createController = function(){ 
      ctrl = $controller('Colecciones2Controller', {
        $scope: $scope
      });
    };
  }));

   it('Validar carga de json', function() {
    //Valida
    createController();
    $httpBackend.flush();
    expect($scope.arrPersonas).toBeDefined();  
  });

  it('Se valida que personas tengan nombre', function() {
    //Valida
    createController();
    $httpBackend.flush();
    expect(_.every($scope.arrPersonas,'name')).toBeTruthy();   
  });

  afterEach(function() {
     $httpBackend.verifyNoOutstandingExpectation();
     $httpBackend.verifyNoOutstandingRequest();
   });

});


describe('App Coleciones', function() {

  var $scope = null;
  var ctrl = null;

  beforeEach(module('app'));

  beforeEach(inject(function($rootScope, $controller) {
    $scope = $rootScope.$new();

    ctrl = $controller('ColeccionesController', {
      $scope: $scope
    });
  }));

  it('Se genera arreglo de personas', function() {
    //Valida
    expect($scope.arrPersonas).toBeDefined();   
  });

  it('Se valida la cantidad de personas', function() {
    //Valida
    expect($scope.arrPersonas.length).toBe(6);   
  });


  it('Se valida que personas tengan nombre', function() {
    //Valida
    expect(_.every($scope.arrPersonas,'name')).toBeTruthy();   
  });



});