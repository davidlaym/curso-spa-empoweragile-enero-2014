
var express = require('express');
var app = module.exports = express();

var http = require('http');

var config = require('./nodeConfig.js')(app, express);



http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});