Code Dojo Angular JS Enero 2014
==================

Código del code dojo.

Estos archivos son los ejemplos que hemos desarrollado en clases, para aprender los distintos conceptos del framework Angular-js

Ejecución
===========

Para ejecutar estos archivos vas a necesitar de Node.Js. Para obtener node.js solo dirigete a la página http://nodejs.org/download/ y descarga los archivos correspondientes a tu sistema operativo y distribución, para luego seguir las instrucciones de instalación que en la misma página aparecen.

Una vez tengas instalado node.js, copia todo el contenido de este repositorio hacia una carpeta local, ya sea clonando el repo o bien descargando el zip. Una vez hecho esto, debes ejecutar `npm install` en la raiz del repo y luego `node nodeServer.js` y con eso tendrás ejecutando el sito web en el puerto 3000 y puedes acceder mediante la url http://localhost:3000

Te recomiendo que hagas un fork del repositorio en vez de solo descargarlo, para que así puedas modificar los ejercicios y compartir con todos nosotros tus descubrimientos o dificultades.

Estructura
============

En la carpeta `/public` es donde está el código angular.js. dentro de esta carpeta está la carpeta `/assets` que contiene librerías y css, todo el resto es material que programamos durante las clases.

Node js
========

Node js es una herramienta de linea de comandos (ejecutable) que permite ejecutar javascript sin un browser de por medio, y darle acceso a toda una gama de recursos que nunca había tenido, como al sistema de archvio o comunicaciones de socket directas. Nodejs además contiene un conjunto de librerías estandar para realizar operaciones de más alto nivel como levantar un server http escuchando en un puerto y otras cosas.

Node js también contiene un administrador de paquetes y dependencias, llamado NPM, el cual te permite rápidamente descargar librerías externas para realizar trabajos de aun más alto nivel, como levantar un sitio web con el patrón de diseño MVC, siendo el más común expressjs. 

No necesitas comprender de Node.Js para entender estos ejemplos, ya que están enfocados en Angular Js y como tal, solo necesitas entender la parte cliente.

En Node.Js solo está implementada una pequeña apí HTTP JSON que permite a la aplicación Angular obtener datos de manera realista.

Internamente lo que realiza es exponer por http los métodos para crear, listar y editar "personas". Para persistencia utiliza archivos json que se ubican en la carpeta `/data` del repositorio.

Esta implementación es muy básica y no demuestra para nada prácticas recomendadas del desarrollo en Node.js, es solamente algo rápido que levanté para poder ejemplificar las comunicaciones HTTP JSON. 

Si quieres aprender sobre Node.JS te recomiendo solicitarnos un curso en donde podamos preparar código útil y consejos de cómo funcionar en este gran framework javascript. 